package oz.revolut

import com.beust.klaxon.Klaxon
import io.ktor.application.*
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.TestApplicationEngine
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import io.mockk.every
import io.mockk.mockk
import org.jetbrains.exposed.dao.EntityID
import oz.revolut.web.*
import kotlin.test.*

val registration1 = UserRegistration("test@test.com","testpassword")
val registration2 = UserRegistration("user@test.com","testpassword")


var user1:User? = null
var user2:User? = null

var accId1:Int = 0
var accId2:Int = 0

class ApplicationTest {

    val userService = mockk<UserService>()
    val accountService = mockk<AccountService>()

    @Test
    fun testCreateUser() = withTestApplication (Application::main){
        val call1 = handleRequest(HttpMethod.Post,"/users") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(Klaxon().toJsonString(registration1))
        }
        with(call1) {
            assertEquals(HttpStatusCode.Created, response.status())
            assertNotNull(response.content)
             user1 = Klaxon().parse<User>(response.content!!)
            assertEquals(user1?.email, registration1.email)
        }
        val call2 = handleRequest(HttpMethod.Post,"/users") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody(Klaxon().toJsonString(registration2))
        }
        with(call2) {
            assertEquals(HttpStatusCode.Created, response.status())
            assertNotNull(response.content)
            user2 = Klaxon().parse<User>(response.content!!)
            assertEquals(user2?.email, registration2.email)
        }


        with(handleRequest(HttpMethod.Get,"/users/"+ user1?.id.toString())){
            assertEquals(HttpStatusCode.OK, response.status())
            assertNotNull(response.content)
            val user:User = Klaxon().parse<User>(response.content!!)!!
            assertEquals(user.accounts?.count(), 3)
            accId1 = user.accounts?.first()?.id!!
        }

        with(handleRequest(HttpMethod.Get,"/users/"+ user2?.id.toString())){
            assertEquals(HttpStatusCode.OK, response.status())
            assertNotNull(response.content)
            val user:User = Klaxon().parse<User>(response.content!!)!!
            assertEquals(user.accounts?.count(), 3)
            accId2 = user.accounts?.first()?.id!!
        }

        with(handleRequest(HttpMethod.Post,"/accounts/"+ accId1.toString()+"/deposit") {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody("""{"amount":"100"}""")
        }){
            assertEquals(HttpStatusCode.OK, response.status())
            assertNotNull(response.content)
            val acc:Account = Klaxon().parse<Account>(response.content!!)!!
            assertEquals(acc.amount, 100)
        }

        with(handleRequest(HttpMethod.Post,"/accounts/"+ accId1.toString()+"/transfer/"+ accId2.toString()) {
            addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())
            setBody("""{"amount":"100"}""")
        }){
            assertEquals(HttpStatusCode.OK, response.status())
            assertNotNull(response.content)
            val acc:Transaction = Klaxon().parse<Transaction>(response.content!!)!!
            assertEquals(acc.status, TransactionStatus.COMPLETED)
        }

        with(handleRequest(HttpMethod.Get,"/users/"+ user1?.id.toString())){
            assertEquals(HttpStatusCode.OK, response.status())
            assertNotNull(response.content)
            val user:User = Klaxon().parse<User>(response.content!!)!!
            val acc:Account = user.accounts?.first()!!
            assertEquals(acc.amount, 0)
        }

        with(handleRequest(HttpMethod.Get,"/users/"+ user2?.id.toString())){
            assertEquals(HttpStatusCode.OK, response.status())
            assertNotNull(response.content)
            val user:User = Klaxon().parse<User>(response.content!!)!!
            val acc:Account = user.accounts?.first()!!
            assertEquals(acc.amount, 100)
        }
    }

    @Test
    fun testGetUser() = testApp {
        every { userService.getById(1) } returns User(1, registration1.email, null)
        with(handleRequest(HttpMethod.Get, "/users/1")){
            assertEquals(response.status(), HttpStatusCode.OK)
            val user = Klaxon().parse<User>(response.content!!)
            print(user)
        }
    }

    /**

     * Private method used to reduce boilerplate when testing the application.

     */
    private fun testApp(callback: TestApplicationEngine.() -> Unit) {
        withTestApplication({ mainWithDeps(userService, accountService) }) { callback() }

    }
}
