package oz.revolut.web

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.auth.*
import com.fasterxml.jackson.databind.*
import io.ktor.jackson.*
import io.ktor.features.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

val accountService:AccountService = AccountService()
val userService:UserService = UserService(accountService)

fun Application.main() {
    // Use inmemory h2 db
    Database.connect(hikari())
    transaction {
        SchemaUtils.create(UsersTable)
        SchemaUtils.create(AccountsTable)
        SchemaUtils.create(TransactionsTable)
    }

    mainWithDeps(userService, accountService)
}

fun Application.mainWithDeps(userService: UserService, accountService: AccountService){
    install(Locations) {
    }

    install(Authentication) {
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    routing{
        users(userService)
        accounts(accountService)
    }
    install(StatusPages){
        exception<BadRequestException> { cause ->
            call.respond(HttpStatusCode.BadRequest, cause.message!!)
        }
        exception<ResourceNotFoundException> { cause ->
            call.respond(HttpStatusCode.NotFound, cause.message!!)
        }
    }


}

