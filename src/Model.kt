package oz.revolut.web

import java.time.temporal.TemporalAmount

data class User(
    val id: Int?,
    val email: String?,
    val accounts: Iterable<Account>?
)

data class Account(
    val id: Int?,
    val currency:Currency?,
    val amount: Int?
)

data class Transaction(
    val status: TransactionStatus,
    val comment: String?
)

data class UserRegistration(
    val email: String,
    val password: String
)

data class AccountDeposit(
    val amount: Int
)