package oz.revolut.web

open class BadRequestException(message:String?): Exception(message)
open class ResourceNotFoundException(message:String?): Exception(message)


class AccountNotFoundException: ResourceNotFoundException("Account not found")
class CurrenciesMismatchException: BadRequestException("Currencies mismatch")
class NotEnoughMoneyException: BadRequestException("Not Enough money")