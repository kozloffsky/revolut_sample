package oz.revolut.web

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

//Tables
object UsersTable: IntIdTable() {
    val email = varchar("email", 120).uniqueIndex()
    val password = varchar("password", length = 120)
}

enum class Currency {
    USD,
    EUR,
    GBP
}

object AccountsTable: IntIdTable() {
    val amount = integer("amount").default(0)
    val currency = enumeration("currency", Currency::class)
    val owner = reference("user", UsersTable)
}

enum class TransactionStatus {
    OPEN,
    COMPLETED,
    FAILED
}
object TransactionsTable: IntIdTable() {
    val amount = integer("amount")
    val senderAccount = reference("sender_account", AccountsTable)
    val targetAccount = reference("target_account", AccountsTable)
    val status = enumeration("status", TransactionStatus::class)
    val comment = varchar("comment", 254).nullable()
}

//DAO
class UserDAO(id: EntityID<Int>): IntEntity(id) {
    companion object: IntEntityClass<UserDAO>(UsersTable)

    var email by UsersTable.email
    var password by UsersTable.password
    val accounts by AccountDAO referrersOn AccountsTable.owner

    fun toModel():User {
        return User(id.value, email, null)
    }

    fun toModel(withAccounts: Boolean): User {
        return transaction { User(id.value, email, accounts.map { it.toModel() }) }
    }
}

class AccountDAO(id: EntityID<Int>): IntEntity(id) {
    companion object: IntEntityClass<AccountDAO>(AccountsTable)

    var amount by AccountsTable.amount
    var currency by AccountsTable.currency
    var owner by UserDAO referencedOn AccountsTable.owner

    fun toModel():Account {
        return Account(id.value, currency, amount)
    }
}

class TransactionDAO(id: EntityID<Int>): IntEntity(id) {
    companion object: IntEntityClass<TransactionDAO>(TransactionsTable)

    var amount by TransactionsTable.amount
    var senderAccount by AccountDAO referencedOn TransactionsTable.senderAccount
    var targetAccount by AccountDAO referencedOn TransactionsTable.targetAccount
    var status by TransactionsTable.status
    var comment by TransactionsTable.comment

    fun toModel():Transaction{
        return Transaction(status, comment)
    }
}

fun hikari(): HikariDataSource {
    val config = HikariConfig()
    config.driverClassName = "org.h2.Driver"
    config.jdbcUrl = "jdbc:h2:mem:test"
    config.maximumPoolSize = 3
    config.isAutoCommit = false
    config.transactionIsolation = "TRANSACTION_REPEATABLE_READ"
    config.validate()
    return HikariDataSource(config)
}

