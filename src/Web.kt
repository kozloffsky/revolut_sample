package oz.revolut.web

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.post
import io.ktor.routing.route


@Location("/{id}")
data class GetByIdRequest(val id: Int)

fun Route.users(userService: UserService) {
    route("/users") {
        get<GetByIdRequest> {
            call.respond(userService.getById(it.id))
        }
        post {
            val dto: UserRegistration = call.receive(UserRegistration::class)
            call.respond(HttpStatusCode.Created, userService.create(dto).toModel())
        }
    }
}

fun Route.accounts(accountService: AccountService) {
    route("/accounts") {
        post("/{id}/deposit") {
            val deposit = call.receive(AccountDeposit::class)
            call.respond(
                accountService.deposit(call.parameters["id"]?.toInt()!!, deposit.amount).toModel())
        }
        post("/{id}/withdraw") {
            val deposit = call.receive(AccountDeposit::class)
            call.respond(
                accountService.withdraw(call.parameters["id"]?.toInt()!!, deposit.amount).toModel())
        }
        post("/{id}/transfer/{target}") {
            val transfer = call.receive(AccountDeposit::class)
            call.respond(
                accountService.transfer(
                    call.parameters["id"]?.toInt()!!,
                    call.parameters["target"]?.toInt()!!,
                    transfer.amount).toModel())

        }
    }
}