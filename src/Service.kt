package oz.revolut.web

import io.ktor.features.NotFoundException
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.temporal.TemporalAmount

class UserService(val accountService: AccountService) {
    fun getById(id: Int):User {
        val userDAO:UserDAO? = transaction { UserDAO.findById(id) }
        if (userDAO == null) {
            throw NotFoundException("User not found")
        }

        return userDAO.toModel(true)
    }

    fun create(dto: UserRegistration):UserDAO {
        val user = transaction {
            UserDAO.new {
                email = dto.email
                password = dto.password
            }
        }

        // Also create accounts for user with zero amount
        Currency.values().forEach {
            accountService.createForUser(user, it)
        }

        return user
    }
}

class AccountService {
    fun getById(id: Int) = transaction {
        AccountDAO.findById(id)
    }

    fun createForUser(user: UserDAO, accountCurrency: Currency) = transaction {
        AccountDAO.new {
            owner = user
            currency = accountCurrency
            amount = 0
        }
    }

    fun deposit(id: Int, amount: Int): AccountDAO {
        val account = transaction { AccountDAO.findById(id) }
        if (account == null) {
            throw AccountNotFoundException()
        }
        transaction {
            account.amount = account.amount + amount
            //"Flushing" values to the database occurs at the end of the transaction
        }

        return account
    }

    fun withdraw(id: Int, amount: Int): AccountDAO {
        val account = getById(id)
        if (account == null) {
            throw AccountNotFoundException()
        }
        transaction {
            if (account.amount - amount < 0) {
                throw NotEnoughMoneyException()
            }
            account.amount = account.amount - amount
            //"Flushing" values to the database occurs at the end of the transaction
        }

        return account
    }

    fun transfer(fromId:Int, toId:Int, amount: Int):TransactionDAO {
        val from = getById(fromId)
        val to = getById(toId)
        if (from == null || to == null){
            throw AccountNotFoundException()
        }
        if (from.currency != to.currency){
            throw CurrenciesMismatchException()
        }

        val trans = beginTransaction(from, to, amount)
        try {
            withdraw(from.id.value, amount)
        }catch (e:NotEnoughMoneyException){
            transaction {
                trans.status = TransactionStatus.FAILED
                trans.comment = e.message
            }
            return trans
        }
        deposit(to.id.value, amount)
        transaction { trans.status = TransactionStatus.COMPLETED }
        return trans
    }

    private fun beginTransaction(from: AccountDAO, to: AccountDAO, transferAmount: Int):TransactionDAO = transaction {
        TransactionDAO.new {
            senderAccount = from
            targetAccount = to
            amount = transferAmount
            status = TransactionStatus.OPEN
        }
    }
}